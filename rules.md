# Rules and Expectations

## Participation
It is expected that all members should frequently particapate with the guild  
We are not looking for solo players uninterested in interacting with their guild  
## Structure and Hierarchy

Upon joining you will be assigned a Prospect Role.  Prospects are guild members
 who have not yet obtained full membership.  The length of time you spend as a 
prospect is completely dependant on you as a person, as moving futher in the 
guild heirarchy requires voting by full members for both entry and removal.

After reviewing the structure and heirarchy of 4Martlock you may be under the 
impression that this is a democtatic community.
You will have noticed many democratic features such as voting rights,
 membership rights, etc. My hope is that we always be able to govern ourselves
 in this way in everything we do but it is important that you understand the
following disclaimer.

# Disclaimer
__I, DonaldClump, keep for myself the final decision in any matter where I see fit.__


