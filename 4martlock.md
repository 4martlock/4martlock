# 4Martlock
4Martlock is a guild designed to play the game as it is described. A full loot,
open world, mmorpg where you are what you wear. We will participate in the
open world as much as possible.

## Info:
* 15% tax
* Timezone: 0-7 UTC

## Requirements:

* Willingness to participate in guild activities on a frequent basis
* Ability to communicate in English
* Discord use, including voice chat


## Rules

