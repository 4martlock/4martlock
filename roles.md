# Roles

## Leadership
* Main leadership only, this should be a very small group with large accesses
* Veto rights

## Council
* Council members should be vetted, knowledgable, committed, and trustworthy
members.
* Should have atleast some form of responsibilities, regear, content creation,
member management, treasury and economy, etc
* Voting rights

## Member
* Members are confirmed, established members of the guilds will full rights.
* Votes required for entry and kicks

## Prospect
* Prospects members under review
* Subject to removal at any time

## 4Martlock
* all above roles should have this role, this is for discord purposes only
not a level of membership

## Hang Around
* Associates are friends and former members who are not in the guild at this
time.
* Limited access above the public

## NO ROLE
* Anyone else, no access except to public channels
