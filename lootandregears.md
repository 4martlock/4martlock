# Loot Split System
All loot obtained from 4Martlock hosted activities should abide by these rules.

All loot should be combined in 1 location by all participating people to be split in 2 of the 2 following systems.

Participants are any players present in the event +1 participant for the guild.

## System 1
- Total listed sell value
- Subtract listing fees 4.5%
- Subtract repair cost
- Subtract seller fee of 10% to obtain subtotal
- Subtotal divded by number of participants

### System 1 Example
- 9 player participants + 1 guild split = 10 total participants
- 10,000,000 total listed sell value
- Subtract listing fees of 4.5% (450,000) = 9,550,000
- Subtract 850,000 total repair cost = 8,700,000
- Seller keeps 10% (870,000) as seller's fee = 7,830,000
- Divide by 10 participants = 783,000 each
- Deposit one split of 783,000 into the guild bank
- Provide each of the 9 participants 1 split of 783,000 silver

## System 2
- Place all items in 1 chest obtain estimated value
- Split non-repaired items evenly among participants
- Deposit 1 split of items into a guild donation tab


#Regear System
It is intended for 4Martlock to have a T7 equivelant regear program for all guild hosted activities as soon as possible. Any activity not intended to be regeared should be explicitly noted.  

There will be a time while a guild balance is built up that will limit the ability to provide regears but, unless it is otherwise noted you should expect all guild activities hosted by 4Martlock leadership, council, or event organizers to be regeared unless explicitly noted.  

It is expected that players utilize an "approved build" to receive a regear. An approved builds list can be located at builds.4martlock.xyz (work in progress)
